FROM phpunit/phpunit:latest
ADD ./ /app
RUN phpunit --configuration ./config.xml