<?php

namespace Volnenko\Commons\Util;

class LinkUtil {

   public static function getRequestURL() {
       if (!isset($_SERVER['REQUEST_URI'])) return '';
       return explode("?", $_SERVER['REQUEST_URI'])[0];
   }

   	public static function getRequestPartsURL() {
   		$url = LinkUtil::getRequestURL();
   		if (empty($url)) return '';
   		return array_filter(preg_split('[/]', $url));
   	}

}