<?php

use PHPUnit\Framework\TestCase;
use Volnenko\Commons\Util\LinkUtil as LinkUtil;

class LinkUtilTest extends TestCase {

    public function testRequestURL() {
        $result = LinkUtil::getRequestURL();
        $this->assertNotNull($result);
    }

    public function testRequestPartsURL() {
        $result = LinkUtil::getRequestPartsURL();
        $this->assertNotNull($result);
    }

}